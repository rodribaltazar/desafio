

## Instalação

Como instalar o projeto do desafio em um ambiente Linux

1. Clone o repositório do projeto.
2. Entre na pasta do projeto <DesafioEagleCare>
3. Você deve estar em um diretório como este: /seu/caminho/desafio/DesafioEagleCare
4. Execute o comando ./vendor/bin/sail up
5. Aguarde o projeto ser montado.
6. Acesse no seu navegador: http://localhost

